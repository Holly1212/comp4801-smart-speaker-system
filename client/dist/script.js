const constraints = {
  audio: {
      channelCount: 1,
      sampleRate: 16000,
      sampleSize: 16,
      volume: 1
  }
}

navigator.mediaDevices.getUserMedia({audio:constraints})
    .then(stream => {handlerFunction(stream)})

function handlerFunction(stream) {
  rec = new MediaRecorder(stream);
  rec.ondataavailable = e => {
    audioChunks.push(e.data);
    if (rec.state == "inactive"){
      let blob = new Blob(audioChunks,{type:'audio/wav;codecs=MS_PCM'});
      var audioURL = URL.createObjectURL(blob);
      console.log(audioURL);
      // recordedAudio.controls=true;
      // recordedAudio.autoplay=true;
      sendData(blob);
      }
    }
  }

function sendData(blob) {
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
  var dateTime = date+'_'+time;
  var filename = dateTime + ".wav";
  var wavfromblob = new File([blob], filename);
  var formData = new FormData();
  formData.append("AudioTitle", "test");
  formData.append("AudioFile", wavfromblob);
  axios.post("http://127.0.0.1:8000/test/", formData,{
    headers: { "Content-Type": "multipart/form-data" },
  })
    .then(function (response) {
      // console.log(response.data);
      if(response.data.asr!= undefined){
        var msg = new SpeechSynthesisUtterance();
        var text = response.data.asr;
        msg.text = text;
        msg.lang = "zh";
        document.getElementById("read").innerHTML = text;
        // window.speechSynthesis.speak(msg);
      }
    })
    .catch(function (response) {
      console.log(response);
    });
}
window.onload = function() {
  // const SW9 = new SiriWave({
  // style: "ios9",
  // container: document.getElementById("container-9"),
  // autostart: true,
  // });
  record = document.getElementById("record");
  stopRecord = document.getElementById("stopRecord");
  record.onclick = e => {
    if(record.getAttribute('recording') == 'F'){ // start recording
      record.setAttribute('recording', 'T');
      audioChunks = [];
      rec.start();
      console.log(rec.state);
    }
    else if(record.getAttribute('recording') == 'T'){ // stop recording
      record.setAttribute('recording', 'F');
      rec.stop();
      console.log(rec.state);
    }
  }
};
