from django import forms

from .models import Audios


class AudioForm(forms.ModelForm):

    class Meta:
        model = Audios
        fields = ['AudioTitle', 'AudioFile']