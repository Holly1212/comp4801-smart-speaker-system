from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse

from .models import Audios
from .forms import AudioForm
from django.views.decorators.csrf import csrf_exempt

import sys
import os.path
import speech_recognition as sr 
from pydub import AudioSegment
from pydub.silence import split_on_silence
import pyaudio
import wave
from array import array
from pathlib import Path
import soundfile as sf
import io
import librosa

from .asr import get_large_audio_transcription

# Create your views here.
@csrf_exempt 
def uploadAudioAPI(request):
    if request.method == 'POST':
        form = AudioForm(request.POST, request.FILES)
        if form.is_valid():
            #form.save()
            handle_uploaded_file(request.FILES['AudioFile'])
            x,_ = librosa.load('input.wav', sr=16000)
            sf.write('tmp.wav', x, 16000)
            #wave.open('tmp.wav','r')
            #input = wave.open("input.wav",'rb')
            # print(request.FILES)
            # blob = request.FILES["AudioFile"]
            # print(type(blob.read()))
            # response = blob.read()
            # s = io.BytesIO(response)
            # print(sys.getsizeof(s))
            # audio = AudioSegment.from_raw(s, sample_width = 1, frame_rate = 16000, channels = 1)
            # text = get_large_audio_transcription(audio)
            # nchannels = 1
            # sampwidth = 1
            # framerate = 16000
            # nframes = 1
            # name = 'output.wav'
            # audio = wave.open(name, 'wb')
            # audio.setnchannels(nchannels)
            # audio.setsampwidth(sampwidth)
            # audio.setframerate(framerate)
            # audio.setnframes(nframes)
            
            # blob = open("input.wav") # such as `blob.read()`
            # audio_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../files/docs/', str(request.FILES["AudioFile"])))
            # data, samplerate = sf.read(audio_path)
            # file_obj = io.BytesIO()  # create file-object
            # file_obj.write(blob.read()) # write in file-object
            # file_obj.seek(0) # move to beginning so it will read from beginning
            #r = sr.Recognizer()
            # mic = sr.AudioFile(blob) # use file-object
            # with sr.AudioFile(blob) as source:
            #     audio = r.record(source)
            # text = get_large_audio_transcription(audio)
            # print(type(mic))
            # r = sr.Recognizer()
            # with mic as source:
            #      audio = r.record(source)
            #      text = r.recognize_google(audio, language="cmn-Hans-CN")

            # data, samplerate = sf.read(audio_path) 
            # r = sr.Recognizer()
            # with sr.WavFile(audio_path) as source:
            #     audio = r.record(source)
            # blob = wave.open(audio_path, 'rb').read()
            # audio.writeframes(blob)
            # audio.close()
            # # audio_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../files/docs/', str(request.FILES["AudioFile"])))
            # # print(audio_path)
            # audio_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "output.wav"))
            # print(audio_path)
            text = get_large_audio_transcription('tmp.wav')
            print("Result:", text)

    #     return HttpResponse("POST OK")
    
    data = [{'name': 'Peter', 'email': 'peter@example.org'},
            {'name': 'Julia', 'email': 'julia@example.org'}]
    return JsonResponse({"asr":text})
    # return HttpResponse("OK")
        #     form = AudioForm(request.POST, request.FILES)
        #     if form.is_valid():
        #         form.save()
        #         return redirect('tutorial_list')
        # else:
        #     form = AudioForm()
        # return render(request, 'tutorial/upload.html', {'form' : form})


def handle_uploaded_file(f):
    with open('input.wav', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)