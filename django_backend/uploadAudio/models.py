from django.db import models

# Create your models here.
class Audios(models.Model):
    AudioTitle = models.CharField(max_length=50)
    AudioFile = models.FileField(upload_to='docs/')

    def __str__(self):
        return self.AudioTitle