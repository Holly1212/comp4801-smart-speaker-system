import os 
import speech_recognition as sr 
from pydub import AudioSegment
from pydub.silence import split_on_silence
import pyaudio
import wave
from array import array
from pathlib import Path

from scipy.io import wavfile
from scipy.io.wavfile import write
import noisereduce as nr
import soundfile as sf
import matplotlib.pyplot as plt
import numpy as np
import io

def speech_recorder():
    
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 16000
    CHUNK = 1024
    RECORD_SECONDS = 30
    WAVE_OUTPUT_FILENAME = "file.wav"

    audio = pyaudio.PyAudio()

    # start Recording
    stream = audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNK)
    print ("Start Recording...")
    frames = []
    silence_count = 0
    
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        data_chunk = array('h',data)
        vol = max(data_chunk)
        frames.append(data)
        
        # break if long silence
        if(vol >= 800):
            silence_count = 0
        else:
            silence_count += 1
            if silence_count == 30:
                print("Finished due to slience.")
                break

    print ("End of recording.")

    # stop Recording
    stream.stop_stream()
    stream.close()
    audio.terminate()

    waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    waveFile.setnchannels(CHANNELS)
    waveFile.setsampwidth(audio.get_sample_size(FORMAT))
    waveFile.setframerate(RATE)
    waveFile.writeframes(b''.join(frames))
    waveFile.close()
    return(WAVE_OUTPUT_FILENAME)

def noise_reduction(data, rate):
    reduced_noise = nr.reduce_noise(y = data, sr=rate, n_std_thresh_stationary=1.5,stationary=True)
    samplerate = 16000
    amplitude = np.iinfo(np.int16).max
    reduced_noise = amplitude * reduced_noise
    
    folder_name = "reduced_noise"
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)
    filename = "rn1.wav"
    rn_filename = os.path.join(folder_name, filename)
    print(rn_filename)
    write(rn_filename, samplerate, reduced_noise.astype(np.int16))
    return rn_filename

def get_large_audio_transcription(path):
    """
    Splitting the large audio file into chunks
    and apply speech recognition on each of these chunks
    """
    r = sr.Recognizer()
    # open the audio file using pydub
    data, rate = sf.read(path)
    data = data
    rate = rate
    reduced_noise_fn = noise_reduction(data, rate)
    # print(reduced_noise_fn)
    # split audio sound where silence is 800 miliseconds or more and get chunks
    sound = AudioSegment.from_wav(reduced_noise_fn)
    
    chunks = split_on_silence(sound,
        min_silence_len = 1000,
        silence_thresh = sound.dBFS-14,
        # keep the silence for 1 second, adjustable as well
        keep_silence=1000,
    )
    folder_name = "audio-chunks"
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)
    
    whole_text = ""
    
    # process each chunk 
    for i, audio_chunk in enumerate(chunks, start=1):
        chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_filename, format="wav")
        # recognize the chunk
        with sr.AudioFile(chunk_filename) as source:
            audio_listened = r.record(source)
            try:
                text = r.recognize_google(audio_listened, language="cmn-Hans-CN")
            except sr.UnknownValueError as e:
                print("Error:", str(e))
            else:
                text = f"{text.capitalize()}. "
                print(chunk_filename, ":", text)
                whole_text += text
    # return the text for all chunks detected
    return whole_text
    
# r = sr.Recognizer()
# path = "data/test.wav"
# print("\nFull text:", get_large_audio_transcription(path))

# r = sr.Recognizer()
# # path = speech_recorder()
# path1 = Path("C:/ZhishengHua/Study/COMP4801/Web Application/django_backend/files/docs/2021-12-16_15-7-56.wav")
# print("\nFull text:", get_large_audio_transcription(path1))