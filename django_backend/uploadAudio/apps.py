from django.apps import AppConfig


class UploadaudioConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uploadAudio'
